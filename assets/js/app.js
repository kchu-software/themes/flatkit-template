$("button").prop('disabled', true);

function login({data: {token, project}}) {
    $.ajax({
        api: false,
        url: 'ajax/save-token.php?token=' + token,
        method: 'GET',
        contentType: 'application/json',
        headers: {
            Authorization: 'Bearer ' + token
        }
    }).done(({data: {redirect}}) => {
        location.href = redirect || './';
    }).fail(({responseJSON, status: code}) => {
        const {message} = responseJSON || {};
        if (code >= 500) {
            toastr.error('Ocurrió un error en la petición, por favor intente mas tarde.');
            console.error(message, responseJSON);
        } else if (code >= 400) {
            toastr.warning(message);
            console.warn(message, responseJSON);
        } else {
            console.error(responseJSON, code);
        }
    });
}
