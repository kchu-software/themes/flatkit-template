let settings = $.get({url: "settings.dev.json", async: false});
if (!settings.responseJSON) {
    settings = $.get({url: "settings.json", async: false});
    if (!settings.responseJSON) {
        console.error('Failed to load settings.json', settings);
    }
}
const {responseJSON: global} = settings;

$(() => {
    $('.parent-module').on('click', (e) => {
        $(e.currentTarget).parent().toggleClass('active');
    });
    let $buttonHTML;
    $(document).on('submit', 'form', (e) => {
        const url = $(e.currentTarget).attr('uri');
        if (url) {
            e.preventDefault();

            const $button = $(`button[type='submit']`);
            $buttonHTML = $button.html();
            $button.prop('disabled', true).prepend('<i class="fa fa-spinner fa-spin"></i>' + ' ');

            const method = $(e.currentTarget).attr('method');
            const callback = $(e.currentTarget).attr('callback');
            let data = $(e.currentTarget).serializeArray();
            if (method.toUpperCase() === 'POST') {
                data = new FormData($(e.currentTarget).get(0));
                $.ajaxSetup({
                    contentType: false,
                    processData: false,
                });
            }

            $.ajax({
                url, method, data,
            }).done((result) => {
                if (window[callback]) {
                    window[callback](result);
                }
            });
        }
    }).on('click', `form button[type='submit']`, (e) => {

    });
    $.ajaxSetup({
        api: true,
        async: true,
        beforeSend: async function (jqXHR, settings) {
            if (settings.api) {
                settings.url = global.apiUrl + settings.url;
            }
        },
        error: function ({responseJSON}) {
            try {
                const {status, code, response, error} = responseJSON || {};
                if (code >= 500) {
                    toastr.error('Ocurrió un error en la petición, por favor intente mas tarde.');
                    console.error(response.message, responseJSON);
                } else if (code >= 400) {
                    toastr.warning(response.message);
                    console.warn(response.message, responseJSON);
                }
            } catch (e) {
                toastr.error('Ocurrió un error en la petición, por favor intente mas tarde.');
                console.error(e, e);
            }
        },
        complete: function () {
            $(`button[type='submit']`).prop('disabled', false).html($buttonHTML);
        }
    });
    if ($.fn.dataTable) {
        $.fn.dataTable.Buttons.defaults.dom.button.className = 'btn btn-sm';
        $.extend(true, $.fn.dataTable.defaults, {
            dom: 'Bfrtip',
            responsive: true,
            stateSave: true,
            order: [[0, 'asc']],
            buttons: [],
            ajax: {
                dataSrc: (name) => {
                    return ({status, code, data, error}) => data[name]
                }
            },
            columnDefs: (columns) => {
                columns.map((column, index) => {
                    column['targets'] = index;
                    return column;
                });
                return columns;
            },
            pageLength: 25,
            language: {
                search: "Buscar:",
                emptyTable: "No hay registros que consultar",
                lengthMenu: "Mostrar _MENU_ registros por pagina",
                info: "Mostrando pagina _PAGE_ de _PAGES_",
                infoEmpty: "Mostrando 0 a 0 de 0 registros",
                loadingRecords: "Cargando...",
                processing: "<i class='fa fa-spin fa-spinner'></i>",
                paginate: {
                    first: "Primero",
                    last: "Ultimo",
                    next: "Siguiente",
                    previous: "Anterior"
                },
            },
        });
        global.dt = $.fn.dataTable.defaults;
    } else {
        console.info('Datatables not found');
    }
    am4core.ready(function () {
        am4core.options.commercialLicense = true;
        am4core.useTheme(am4themes_animated);
    }); // end am4core.ready()
});

function checkValidity(e) {
    const $form = $(e.currentTarget).closest('form');
    if ($form.length) {
        return $(e.currentTarget).closest('form')[0].checkValidity();
    }
}

function loadChart($element, data, options) {
    // Create chart instance
    const chart = am4core.create($element.get(0), am4charts.XYChart);
    chart.language.locale = am4lang_es_ES;
    chart.language.locale["_decimalSeparator"] = ".";
    chart.language.locale["_thousandSeparator"] = ",";

    // Add data
    chart.data = data;

    // Create axes
    const dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

    // Set input format for the dates
    chart.dateFormatter.inputDateFormat = "yyyy-MM-dd H:mm";
    dateAxis.groupData = false;
    dateAxis.dateFormats.setKey("minute", "MMMM d, yyyy H:mm");
    dateAxis.baseInterval = {
        "timeUnit": "minute",
        "count": 1
    }

    dateAxis.min = (new Date(2018, 10, 1)).getTime();

    // Create series
    const series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = "value";
    series.dataFields.dateX = "date";
    series.tooltipText = "{value}"
    series.strokeWidth = 2;
    series.minBulletDistance = 15;

    // Drop-shaped tooltips
    series.tooltipText = '$' + `{value.formatNumber('#,##0.00')}`;
    series.tooltip.background.cornerRadius = 20;
    series.tooltip.background.strokeOpacity = 0;
    series.tooltip.pointerOrientation = "vertical";
    series.tooltip.label.minWidth = 40;
    series.tooltip.label.minHeight = 40;
    series.tooltip.label.textAlign = "middle";
    series.tooltip.label.textValign = "middle";

    // Make bullets grow on hover
    const bullet = series.bullets.push(new am4charts.CircleBullet());
    bullet.circle.strokeWidth = 2;
    bullet.circle.radius = 4;
    bullet.circle.fill = am4core.color("#fff");

    const bullethover = bullet.states.create("hover");
    bullethover.properties.scale = 1.3;

    // Make a panning cursor
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "panXY";
    chart.cursor.xAxis = dateAxis;
    chart.cursor.snapToSeries = series;

    // Create vertical scrollbar and place it before the value axis
    chart.scrollbarY = new am4core.Scrollbar();
    chart.scrollbarY.parent = chart.leftAxesContainer;
    chart.scrollbarY.toBack();

    // Create a horizontal scrollbar with previe and place it underneath the date axis
    chart.scrollbarX = new am4charts.XYChartScrollbar();
    chart.scrollbarX.series.push(series);
    chart.scrollbarX.parent = chart.bottomAxesContainer;
}
